# Changelog

This file only reflects the changes that are made in this image. Please refer to the upstream GitLab [CHANGELOG](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CHANGELOG) for the list of changes in GitLab.

**8.8.1-zh1**
- gitlab: upgrade to CE v8.8.1
- 根据larryli提供的汉化版本进行构建

**8.7.3-zh1**
- gitlab: upgrade to CE v8.7.3
- 根据larryli提供的汉化版本进行构建

**8.7.2**
- gitlab: upgrade to CE v8.7.2
